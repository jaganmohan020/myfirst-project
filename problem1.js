function carDetails(inventory,id){

    if(Array.isArray(inventory) && inventory.length != 0 && isNaN(id) !== true){
        for(let index = 0; index < inventory.length; index++){
            
            if(inventory[index].id == id){
                return [inventory[index]];
            }
        }
   
    } else{
        return [];
    }
}
    
module.exports = carDetails;